#include <iostream>
#include <rttr/type>
#include <Base/StreamColor.hpp>

#ifdef NDEBUG
static rttr::string_view gameCoreLibName("GameCore");
#else
static rttr::string_view gameCoreLibName("GameCore_d");
#endif

int main(int argc, char* argv[])
{
	using namespace rttr;

	std::cout << Core::LogWithColor("32", "----------------------------- Playing PlanetGame --------------------------------------") << std::endl;

	library lib(gameCoreLibName); // load the GameCore plugin

	if (!lib.load())
	{
		std::cerr << Core::LogWithColor("31", lib.get_error_string()) << std::endl;
		return -1;
	}

	{
		
	}

	if (!lib.unload())
	{
		std::cerr << Core::LogWithColor("31", lib.get_error_string()) << std::endl;
		return -1;
	}

	std::cout << Core::LogWithColor("32", "----------------------------- Played PlanetGame ----------------------------------------") << std::endl;

	return 0;
}