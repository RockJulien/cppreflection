#pragma once

#include <stack>
#include <Exceptions/Exceptions.hpp>

namespace Core
{
	namespace Containers
	{
		/// <summary>
		/// Author: jlarbi
		/// 
		/// Thread safe task using an underlying std::stack.
		/// </summary>
		/// <typeparam name="T">The item contained type.</typeparam>
		template<typename T>
		class ConcurrentStack : public IObject
		{
		public:

			typedef std::shared_ptr<T> ItemPtr;
			typedef T& RefItem;
			typedef T Item;

		private:

			typedef std::stack<T> Data;			

			Data mData;
			mutable std::mutex mSyncRoot;

		public:

			ConcurrentStack() = default;

			ConcurrentStack(const ConcurrentStack& other)
			{
				std::lock_guard<std::mutex> lock(other.mSyncRoot);

				// copy in constructor body rather than 
				// the member initializer list 
				// in order to ensure that the mutex is held across the copy.
				mData = other.mData;
			}

			ConcurrentStack& operator=(const ConcurrentStack&) = delete;

			void Push(typename Item newItem)
			{
				std::lock_guard<std::mutex> lock(mSyncRoot);

				mData.push(newItem);
			}

			typename ItemPtr Pop()
			{
				std::lock_guard<std::mutex> lock(mSyncRoot);

				// check for empty before trying to pop value
				if (mData.empty()) 
					throw Exceptions::IsEmptyException();

				// allocate return value before modifying stack
				std::shared_ptr<T> const result(std::make_shared<T>(mData.top()));

				mData.pop();

				return result;
			}

			void Pop(typename RefItem outValue)
			{
				std::lock_guard<std::mutex> lock(mSyncRoot);

				if (mData.empty()) 
					throw Exceptions::IsEmptyException();

				outValue = mData.top();

				mData.pop();
			}

			bool IsEmpty() const
			{
				std::lock_guard<std::mutex> lock(mSyncRoot);

				return mData.empty();
			}

			RTTR_ENABLE(IObject)
			RTTR_REGISTRATION_FRIEND
		};
	}
}