#pragma once

#include <unordered_set>
#include <Exceptions/Exceptions.hpp>

namespace Core
{
	namespace Containers
	{
		/// <summary>
		/// Author: jlarbi
		/// 
		/// Hashset implementation using an underlying std::unordered_set.
		/// </summary>
		/// <typeparam name="TItem"></typeparam>
		template<typename TItem>
		class Hashset : public IObject
		{
		public:

			typedef std::unordered_set<TItem> Data;
			//typedef std::unordered_set<TItem>::iterator Data_Iterator;

		private:

			Data mData;

		public:

			Hashset() = default;

			Hashset(std::initializer_list<TItem> list)
			{
				for (auto item : list)
				{
					Add(item);
				}
			}

			Hashset(const Hashset& other)
			{
				mData = other.mData;
			}

			Hashset(Hashset&& other) :
			mData(std::move(other.mData))
			{
				
			}

			/*
			 Gets the set element count.
			 return: The amount of element in the set.
			*/
			unsigned int Count() const
			{
				return (unsigned int)mData.size();
			}

			/*
			 Adds a new element to the set.
			 newItem: The item to add.
			 return: True if succeeded, false otherwise.
			*/
			bool Add(const TItem& newItem)
			{
				const auto& result = mData.insert(newItem);
				return result.second;
			}

			/*
			 Adds a new element to the set.
			 newItem: The item to add.
			 return: True if succeeded, false otherwise.
			*/
			bool Add(TItem&& newItem)
			{
				const auto& result = mData.insert(std::move(newItem));
				return result.second;
			}

			/*
			 Removes an element from the set.
			 toRemove: The item to remove.
			 return: True if succeeded, false otherwise.
			*/
			bool Remove(TItem toRemove)
			{
				const auto& result = mData.erase(toRemove);
				return result == 1;
			}

			/*
			 Determine if this and other caches contains at least one element in common.
			 other: The other cache to test.
			 return: True if at least one element in common, false otherwise.
			*/
			bool Overlaps(const Hashset& other)
			{
				const int thisCount = this->Count();
				const int otherCount = other.Count();

				const Data* cacheToIterate = nullptr;
				const Data* cacheToTestAgainst = nullptr;
				if (thisCount > otherCount)
				{
					cacheToIterate = &other.mData;
					cacheToTestAgainst = &this->mData;
				}
				else
				{
					cacheToIterate = &this->mData;
					cacheToTestAgainst = &other.mData;
				}

				for (auto i = cacheToIterate->begin(); i != cacheToIterate->end(); i++)
				{
					if (cacheToTestAgainst->find(*i) != cacheToTestAgainst->end())
					{
						return true;
					}
				}

				return false;
			}

			/*
			 Remove all matching element from other that would be in 
			 this cache.
			 other: The cache containing elements to remove if contained in this cache.
			*/
			void ExceptWith(const Hashset& other)
			{
				for (auto item : other.mData)
				{
					this->mData.erase(item);
				}
			}

			/*
			 Merge this elements and other's elements so that this contains
			 elements from both without duplicates.
			 other: The other cache to union with.
			*/
			void UnionWith(const Hashset& other)
			{
				Data unionData;
				std::set_union(this->mData.begin(), this->mData.end(), 
							   other.mData.begin(), other.mData.end(), 
							   std::inserter(unionData, unionData.begin()));

				this->mData = std::move(unionData);
			}

			/*
			 Modify this cache so it contains only the common elements between this and other.
			 other: The other cache to test.
			*/
			void IntersectWith(const Hashset& other)
			{
				const int thisCount = this->Count();
				const int otherCount = other.Count();
				
				Data commonOnly;
				const Data* cacheToIterate = nullptr;
				const Data* cacheToTestAgainst = nullptr;
				if (thisCount > otherCount)
				{
					cacheToIterate = &other.mData;
					cacheToTestAgainst = &this->mData;
				}
				else
				{
					cacheToIterate = &this->mData;
					cacheToTestAgainst = &other.mData;
				}
				
				for (auto i = cacheToIterate->begin(); i != cacheToIterate->end(); i++)
				{
					if (cacheToTestAgainst->find(*i) != cacheToTestAgainst->end())
					{
						commonOnly.insert(*i);
					}
				}

				this->mData = std::move(commonOnly);
			}

			/*
			 Clear the set.
			*/
			void Clear()
			{
				mData.clear();
			}

			class Iterator
			{
			public:

				using iterator_category = std::forward_iterator_tag;
				using difference_type = std::ptrdiff_t;
				using value_type = TItem;
				using pointer = value_type*;  // or also value_type*
				using reference = value_type&;  // or also value_type&
				using const_pointer = const pointer;
				using const_reference = const reference;

				Iterator(typename std::unordered_set<TItem>::iterator wrappedIterator) :
				mWrappedIterator(wrappedIterator)
				{
					
				}

				const TItem& operator*() const { return *mWrappedIterator; }
				//const TItem* operator->() { return mWrappedIterator; }

				// Prefix increment
				Iterator& operator++() { mWrappedIterator++; return *this; }

				// Postfix increment
				Iterator operator++(int) { Iterator tmp = *this; ++(*this); return tmp; }

				friend bool operator== (const Iterator& a, const Iterator& b) { return a.mWrappedIterator == b.mWrappedIterator; };
				friend bool operator!= (const Iterator& a, const Iterator& b) { return a.mWrappedIterator != b.mWrappedIterator; };


			private:

				typename std::unordered_set<TItem>::iterator mWrappedIterator;
			};

			Iterator begin()
			{
				return Iterator(mData.begin());
			}

			Iterator end()
			{ 
				return Iterator(mData.end());
			}
		};
	}
}