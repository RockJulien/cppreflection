#include "Event.hpp"

#include <rttr/registration.h>

using namespace Core;

RTTR_PLUGIN_REGISTRATION
{
    // Involved namespace(s).
    using namespace rttr;

	// Indicate what the class is made of for reflection potential purpose on it.
    
	registration::class_<Event<>>("Core::Event<>")
					.constructor<>(registration::public_access)
					.method("Add", select_overload<typename Event<>::HandlerType::HandlerIdentifier(const Event<>::HandlerType& handler)>(&Event<>::Add), registration::public_access)
					.method("Add", select_overload<typename Event<>::HandlerType::HandlerIdentifier(const typename Event<>::HandlerType::Delegate& handler)>(&Event<>::Add), registration::public_access)
					.method("Remove", &Event<>::Remove, registration::public_access)
					.method("RemoveById", &Event<>::RemoveById, registration::public_access)
					.method("Invoke", &Event<>::Invoke, registration::public_access)
					//.method("BeginInvoke", &Event<>::BeginInvoke, registration::public_access)
					;
}