#pragma once

#include "EventHandler.hpp"

namespace Core
{
	/// <summary>
	/// Author: jlarbi
	/// 
	/// Event class allowing to create C# like event member(s) in any class.
	/// </summary>
	/// <typeparam name="...Args"></typeparam>
	template <typename... Args> 
	class Event : public IObject
	{
	public:
		typedef EventHandler<Args...> HandlerType;
	protected:
		typedef std::list<HandlerType> HandlerCollection;

	private:

		HandlerCollection mHandlers;
		mutable std::mutex mSyncRoot;

	protected:

		void InternalInvoke(const HandlerCollection& handlers, Args... params) const
		{
			for (const auto& handler : handlers)
			{
				handler(params...);
			}
		}

		HandlerCollection GetHandlersCopy() const
		{
			std::lock_guard<std::mutex> lock(mSyncRoot);

			// Since the function return value is by copy, 
			// before the function returns (and destruct the lock_guard object),
			// it creates a copy of the mHandlers container.

			return mHandlers;
		}

	public:
		
		Event() = default;

		// copy constructor
		Event(const Event& src)
		{
			std::lock_guard<std::mutex> lock(src.mSyncRoot);

			mHandlers = src.mHandlers;
		}

		// move constructor
		Event(Event&& src)
		{
			std::lock_guard<std::mutex> lock(src.mSyncRoot);

			mHandlers = std::move(src.mHandlers);
		}

		virtual ~Event()
		{
			mHandlers.clear();
		}

		// copy assignment operator
		Event& operator=(const Event& src)
		{
			std::lock_guard<std::mutex> lock(mSyncRoot);
			std::lock_guard<std::mutex> lock2(src.mSyncRoot);

			mHandlers = src.mHandlers;

			return *this;
		}

		// move assignment operator
		Event& operator=(Event&& src)
		{
			std::lock_guard<std::mutex> lock(mSyncRoot);
			std::lock_guard<std::mutex> lock2(src.mSyncRoot);

			std::swap(mHandlers, src.mHandlers);

			return *this;
		}

		typename HandlerType::HandlerIdentifier Add(const HandlerType& handler)
		{
			std::lock_guard<std::mutex> lock(mSyncRoot);

			mHandlers.push_back(handler);

			return handler.Id();
		}

		inline typename HandlerType::HandlerIdentifier Add(const typename HandlerType::Delegate& handler)
		{
			return Add(HandlerType(handler));
		}

		bool Remove(const HandlerType& handler)
		{
			std::lock_guard<std::mutex> lock(mSyncRoot);

			auto it = std::find(mHandlers.begin(), mHandlers.end(), handler);
			if (it != mHandlers.end())
			{
				mHandlers.erase(it);
				return true;
			}

			return false;
		}

		bool RemoveById(const typename HandlerType::HandlerIdentifier& handlerId)
		{
			std::lock_guard<std::mutex> lock(mSyncRoot);

			auto it = std::find_if(mHandlers.begin(), mHandlers.end(),
				[handlerId](const HandlerType& handler) { return handler.Id() == handlerId; });
			if (it != mHandlers.end())
			{
				mHandlers.erase(it);
				return true;
			}

			return false;
		}

		void Invoke(Args... params) const
		{
			HandlerCollection handlersCopy = GetHandlersCopy();

			InternalInvoke(handlersCopy, params...);
		}

		std::future<void> BeginInvoke(Args... params) const
		{
			return std::async(std::launch::async, [this](Args... asyncParams) { Invoke(asyncParams...); }, params...);
		}

		inline void operator()(Args... params) const
		{
			Invoke(params...);
		}

		inline typename HandlerType::HandlerIdentifier operator+=(const HandlerType& handler)
		{
			return Add(handler);
		}

		inline typename HandlerType::HandlerIdentifier operator+=(const typename HandlerType::Delegate& handler)
		{
			return Add(handler);
		}

		inline bool operator-=(const HandlerType& handler)
		{
			return Remove(handler);
		}

		RTTR_ENABLE(IObject)
		RTTR_REGISTRATION_FRIEND
	};
}