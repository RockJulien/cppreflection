#include "Timer.hpp"

#include <rttr/registration.h>
#include <iostream>

using namespace Core;

Timer::Timer(bool isLooped, unsigned int intervalInMilliseconds) :
mIsLooped(isLooped), mIsRunning(false), mIsStarted(false), 
mIntervalInMilliseconds(intervalInMilliseconds > 0 ? intervalInMilliseconds : DEFAULT_TIMER_INTERVAL)
{

}

Timer::~Timer()
{
	if (mIsStarted)
	{
		if (mTimerThread.joinable())
		{
			mTimerThread.join();
		}
	}
}

bool Timer::IsRunning() const
{
	return mIsRunning;
}

bool Timer::Start()
{
	if (mIsRunning)
	{
		return false;
	}

	mIsRunning = true;
	mTimerThread = std::thread([this]() { OnTimerTicked(); });
	mIsStarted = true;

	return true;
}

bool Timer::Stop()
{
	if (!mIsRunning)
	{
		return false;
	}

	std::lock_guard<std::mutex> lock(mSyncRoot);

	mIsRunning = false;

	mRunCondition.notify_all();

	if (mTimerThread.joinable())
	{
		mTimerThread.join();
	}

	mIsStarted = false;

	return true;
}

void Timer::OnTimerTicked()
{
	while (mIsRunning)
	{
		std::unique_lock<std::mutex> lock(mSyncRoot);
		auto waitResult = mRunCondition.wait_for(lock, std::chrono::milliseconds(mIntervalInMilliseconds), [this] { return !mIsRunning; });
		//std::this_thread::sleep_for(std::chrono::milliseconds(mIntervalInMilliseconds));

		if (mIsRunning && !waitResult)
		{
			Tick();
		}

		if (!mIsLooped)
		{
			mIsRunning = false;
		}
	}
}

RTTR_PLUGIN_REGISTRATION
{
    // Involved namespace(s).
    using namespace rttr;

	// Indicate what the class is made of for reflection potential purpose on it.
    
	registration::class_<Timer>("Core::Timer")
					.constructor<bool, unsigned int>(registration::public_access)
					(
						default_arguments(true, 1000u),
						parameter_names("isLooped", "intervalInMilliseconds")
					)
					.method("Start", &Timer::Start, registration::public_access)
					.method("Stop", &Timer::Stop, registration::public_access)
					.property_readonly("IsRunning", &Timer::IsRunning, registration::public_access)
					;
}