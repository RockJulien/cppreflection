#pragma once

#include "Event.hpp"

namespace Core
{
	/// <summary>
	/// Author: jlarbi
	/// 
	/// Timer class allowing to get tick each given amount of time.
	/// </summary>
	class GAMECORE_EXPORT Timer : public IObject
	{
	private:

		const bool mIsLooped;
		bool mIsStarted;
		std::atomic_bool mIsRunning;
		unsigned int mIntervalInMilliseconds;
		std::thread mTimerThread;
		/*mutable*/ std::mutex mSyncRoot;
		std::condition_variable mRunCondition;

		void OnTimerTicked();

	public:

		constant unsigned int DEFAULT_TIMER_INTERVAL = 1000;

		Event<> Tick;

	public:

		Timer(bool isLooped = true, unsigned int intervalInMilliseconds = DEFAULT_TIMER_INTERVAL);
		virtual ~Timer();

		/// <summary>
		/// Gets the flag indicating whether the timer is running or not.
		/// </summary>
		/// <returns>True if running, false otherwise.</returns>
		bool IsRunning() const;

		/// <summary>
		/// Start the timer.
		/// </summary>
		/// <returns>True is started properly, false otherwise.</returns>
		bool Start();

		/// <summary>
		/// Stops the timer.
		/// </summary>
		/// <returns>True is stopped properly, false otherwise.</returns>
		bool Stop();

		RTTR_ENABLE(IObject)
		RTTR_REGISTRATION_FRIEND
	};
}