#include "EventHandler.hpp"

#include <rttr/registration.h>

using namespace Core;

RTTR_PLUGIN_REGISTRATION
{
    // Involved namespace(s).
    using namespace rttr;

	// Indicate what the class is made of for reflection potential purpose on it.
    
	registration::class_<EventHandler<>>("Core::EventHandler<>")
					.constructor<const EventHandler<>::Delegate&>(registration::public_access)
					.property_readonly("Id", &EventHandler<>::Id, registration::public_access);
}