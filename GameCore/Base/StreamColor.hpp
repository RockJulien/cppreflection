#pragma once

#include "IObject.hpp"

namespace Core
{
	/// <summary>
	/// Author: jlarbi
	/// 
	/// Note:
	///               foreground background
	///     black        30         40
	/// 	red          31         41
	/// 	green        32         42
	/// 	yellow       33         43
	/// 	blue         34         44
	/// 	magenta      35         45
	/// 	cyan         36         46
	/// 	white        37         47
	/// 
	/// More option(s):
	/// 
	///     reset             0  (everything back to normal)
	///     bold / bright     1  (often a brighter shade of the same colour)
	/// 	underline         4
	/// 	inverse           7  (swap foreground and background colours)
	/// 	bold / bright off 21
	/// 	underline off     24
	/// 	inverse off       27
	/// 
	/// See more details : https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
	/// </summary>
	/// <typeparam name="ItemT">The item to write.</typeparam>
	/// <typeparam name="CharT">The color string.</typeparam>
	template <typename ItemT, typename CharT> 
	class StreamColor : public IObject
	{
	private:
		const CharT* mColorString;
		const ItemT& mItem;

	public:
		StreamColor(const CharT* colorStr, const ItemT& item) : 
		mColorString(colorStr), mItem(item)
		{
		}

		// Disable Copy
		StreamColor(const StreamColor& other) = delete;
		StreamColor& operator=(const StreamColor& other) = delete;

		// Enable Move
		StreamColor(StreamColor&& other) : 
		mColorString(other.mColorString), mItem(other.mItem)
		{
		}

		// The parameter is an R-Value reference since we don't want retained copies of this class.
		friend std::basic_ostream<CharT>& operator<<(std::basic_ostream<CharT>& os, StreamColor&& item)
		{
			static const CharT strPrefix[3]{ '\x1b', '[', '\0' };
			static const CharT strSuffix[5]{ '\x1b', '[', '0', 'm', '\0' };

			os << strPrefix << item.mColorString << CharT('m') << item.mItem << strSuffix;

			return os;
		}

		RTTR_ENABLE(IObject)
		RTTR_REGISTRATION_FRIEND
	};

	template <typename ItemT, typename CharT> 
	StreamColor<ItemT, CharT> LogWithColor(const CharT* colorStr, const ItemT& item)
	{
		return StreamColor<ItemT, CharT>(colorStr, item);
	}
}