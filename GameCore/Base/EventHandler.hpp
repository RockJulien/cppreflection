#pragma once

#include "IObject.hpp"

namespace Core
{
	/// <summary>
	/// Author: jlarbi
	/// 
	/// Event handler class owning ref to the function to call
	/// </summary>
	/// <typeparam name="...Args">Optional arguments.</typeparam>
	template <typename... Args> 
	class EventHandler : public IObject
	{
	public:
		typedef std::function<void(Args...)> Delegate;
		typedef unsigned int HandlerIdentifier;

	private:

		static std::atomic_uint mHandleIdCounter;
		Delegate mDelegate;
		HandlerIdentifier mId;

	public:
		
		explicit EventHandler(const Delegate& action) : 
		mDelegate(action)
		{
			mId = ++mHandleIdCounter;
		}

		// copy constructor
		EventHandler(const EventHandler& src) : 
		mDelegate(src.mDelegate), mId(src.mId)
		{

		}

		// move constructor
		EventHandler(EventHandler&& src) : 
		mDelegate(std::move(src.mDelegate)), mId(src.mId)
		{

		}

		virtual ~EventHandler()
		{
			mDelegate = nullptr;
		}

		// copy assignment operator
		EventHandler& operator=(const EventHandler& src)
		{
			mDelegate = src.mDelegate;
			mId = src.mId;

			return *this;
		}

		// move assignment operator
		EventHandler& operator=(EventHandler&& src)
		{
			std::swap(mDelegate, src.mDelegate);
			mId = src.mId;

			return *this;
		}

		// function call operator
		void operator()(Args... params) const
		{
			if (mDelegate)
			{
				mDelegate(params...);
			}
		}

		bool operator==(const EventHandler& other) const
		{
			return mId == other.mId;
		}

		operator bool() const
		{
			return mDelegate;
		}

		HandlerIdentifier Id() const
		{
			return mId;
		}

		RTTR_ENABLE(IObject)
		RTTR_REGISTRATION_FRIEND
	};

	template <typename... Args> 
	std::atomic_uint EventHandler<Args...>::mHandleIdCounter(0);
}