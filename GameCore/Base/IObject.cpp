#include "IObject.hpp"
#include <rttr/rttr_cast.h>

#include <rttr/registration.h>

using namespace Core;

IObject::~IObject()
{
	// Even pure virtual destructor require a body in all cases
	// Due to how c++ destruction behaves and dtor being not really overriden but just call in reverse order
	// of the class derivation / construction and must exist anyway.
}

rttr::type IObject::GetType() const
{
	return rttr::type::get(*this);
}

std::string IObject::ToString() const
{
	return this->GetType().get_name().to_string();
}

bool IObject::Equals(const IObject* other) const
{
	if (other == nullptr)
	{
		return false;
	}

	// Ref test equality by default.
	return this == other;
}

rttr::type IObject::GetType(const std::string& typeName)
{
	return rttr::type::get_by_name(typeName);
}

bool Core::operator==(const IObject& first, const IObject& second)
{
	return first.Equals(&second);
}

bool Core::operator!=(const IObject& first, const IObject& second)
{
	return !(first == second);
}

RTTR_PLUGIN_REGISTRATION
{
    // Involved namespace(s).
    using namespace rttr;

	// Indicate what the class is made of for reflection potential purpose on it.
	registration::class_<IObject>("Core::IObject")
					//.constructor<>(registration::protected_access)
					.method("GetType", select_const(&IObject::GetType), registration::public_access)
					.method("ToString", &IObject::ToString, registration::public_access)
					.method("Equals", &IObject::Equals, registration::public_access)
					(
						parameter_names("other") // provide the names of the parameter(s);
					);
}
