#pragma once

#include <Constants/Constants.hpp>

namespace Core
{
	/// <summary>
	/// Author: jlarbi
	/// 
	/// Base strong type object interface
	/// </summary>
	class GAMECORE_EXPORT IObject
	{
	protected:
		IObject() = default;

	public:

		virtual ~IObject() = 0;

		template<typename T>
		inline bool Is() const
		{
			return this->GetType().is_derived_from<T>();
		}

		template<typename T>
		inline T As() const
		{
			return rttr::rttr_cast<T>(this);
		}
		
		inline rttr::type GetType() const;

		virtual std::string ToString() const;

		virtual bool Equals(const IObject* other) const;

		static rttr::type GetType(const std::string& typeName);

		friend bool operator == (const IObject& first, const IObject& second);
		friend bool operator != (const IObject& first, const IObject& second);

		RTTR_ENABLE()
		RTTR_REGISTRATION_FRIEND
	};
}