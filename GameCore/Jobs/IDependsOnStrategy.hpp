#pragma once

#include <Base/IObject.hpp>

namespace Core
{
	namespace Jobs
	{
		/// <summary>
		/// Author: jlarbi
		/// 
		/// Job depends on strategy interface that replace class attribute.
		/// </summary>
		class IDependsOnAttribute : public IObject
		{
		protected:

			IDependsOnAttribute() = default;

		public:

			virtual ~IDependsOnAttribute() = 0;



			RTTR_ENABLE(IObject)
			RTTR_REGISTRATION_FRIEND
		};
	}
}