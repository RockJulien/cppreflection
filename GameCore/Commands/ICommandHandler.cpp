#include "ICommandHandler.hpp"

#include <rttr/registration.h>

using namespace Core::Commands;

RTTR_PLUGIN_REGISTRATION
{
    // Involved namespace(s).
    using namespace rttr;

    // Indicate what the class is made of for reflection potential purpose on it.
    registration::class_<ICommandHandler>("Core::Commands::ICommandHandler");
                    //.constructor<>(registration::protected_access);
}