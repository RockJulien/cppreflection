#pragma once

#include <Base/IObject.hpp>

namespace Core
{
	namespace Commands
	{
		/// <summary>
		/// Author: jlarbi
		/// 
		/// Base command handler strong type interface to implement to
		/// be able to handle ICommand.
		/// </summary>
		class ICommandHandler : public IObject
		{
		protected:
			ICommandHandler() = default;

		public:

			virtual ~ICommandHandler() = 0;

			RTTR_ENABLE(IObject)
			RTTR_REGISTRATION_FRIEND
		};
	}
}