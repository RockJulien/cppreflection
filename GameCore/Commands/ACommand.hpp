#pragma once

#include <ICommand.hpp>

namespace Core
{
	namespace Commands
	{
		/// <summary>
		/// Author: jlarbi
		/// 
		/// Base command implem.
		/// </summary>
		class ACommand : public ICommand
		{
		protected:
			ACommand() = default;

		public:

			virtual ~ACommand() = 0;

			RTTR_ENABLE(ICommand)
			RTTR_REGISTRATION_FRIEND
		};
	}
}