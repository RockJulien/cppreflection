#pragma once

#include <Base/IObject.hpp>

namespace Core
{
	namespace Commands
	{
		/// <summary>
		/// Author: jlarbi
		/// 
		/// Base command interface to implement by any class intended to be a command of any commandhandler.
		/// Command can be processed by N commandHandler, so no need to create different command for different commandhander
		/// as pushing one command handled by N commandHandler will reach each of them auto.
		/// </summary>
		class ICommand : public IObject
		{
		protected:
			ICommand() = default;

		public:

			virtual ~ICommand() = 0;

			RTTR_ENABLE(IObject)
			RTTR_REGISTRATION_FRIEND
		};
	}
}