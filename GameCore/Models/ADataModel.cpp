#include "ADataModel.hpp"

#include <rttr/registration.h>

using namespace Core::Models;

std::string ADataModel::GetName() const
{
	return this->GetType().get_name().to_string();
}

void ADataModel::Reset(const std::string& reason)
{
    OnReset(reason);
}

void ADataModel::OnReset(const std::string& reason)
{
    // Nothing to do yet.
}

RTTR_PLUGIN_REGISTRATION
{
    // Involved namespace(s).
    using namespace rttr;

    // Indicate what the class is made of for reflection potential purpose on it.
    registration::class_<ADataModel>("Core::Models::ADataModel")
                    .method("OnReset", &ADataModel::OnReset, registration::protected_access)
                    (
                        parameter_names("reason")
                    );
}