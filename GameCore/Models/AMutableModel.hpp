#pragma once

#include "ADataModel.hpp"
#include "IMutableModel.hpp"

namespace Core
{
	namespace Models
	{
		/// <summary>
		/// Author: jlarbi
		/// 
		/// Base abstract class for all state models (mutable over app lifetime)
		/// </summary>
		class AMutableModel : public ADataModel, public IMutableModel
		{
		protected:

			AMutableModel() = default;

		public:

			virtual ~AMutableModel() = 0;

			/// <summary>
			/// Gets the flag indicating whether the model is a config one or not.
			/// </summary>
			virtual bool IsConfig() const override final;

			RTTR_ENABLE(ADataModel, IMutableModel)
			RTTR_REGISTRATION_FRIEND
		};
	}
}