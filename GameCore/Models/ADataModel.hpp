#pragma once

#include "IConfigModel.hpp"

namespace Core
{
	namespace Models
	{
		/// <summary>
		/// Author: jlarbi
		/// 
		/// Base abstract class for all data models.
		/// </summary>
		class ADataModel : public virtual IDataModel
		{
		protected:

			ADataModel() = default;

			/// <summary>
			/// Reset the service and its data if needed.
			/// </summary>
			/// <param name="reason">The reset reason.</param>
			virtual void OnReset(const std::string& reason);

		public:

			virtual ~ADataModel() = 0;

			/// <summary>
			/// Gets the model's name.
			/// </summary>
			virtual std::string GetName() const override;

			/// <summary>
			/// Reset the model and its data.
			/// </summary>
			/// <param name="reason">The reset reason.</param>
			virtual void Reset(const std::string& reason) override final;

			RTTR_ENABLE(IDataModel)
			RTTR_REGISTRATION_FRIEND
		};
	}
}