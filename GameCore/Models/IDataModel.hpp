#pragma once

#include <Base/IObject.hpp>

namespace Core
{
	namespace Models
	{
		/// <summary>
		/// Author: jlarbi
		/// 
		/// Base data model interface to implement.
		/// </summary>
		class IDataModel : public virtual IObject
		{
		protected:

			IDataModel() = default;

		public:

			virtual ~IDataModel() = 0;

			/// <summary>
			/// Gets the model's name.
			/// </summary>
			virtual std::string GetName() const = 0;

			/// <summary>
			/// Gets the flag indicating whether the model is a config one or not.
			/// </summary>
			virtual bool IsConfig() const = 0;

			/// <summary>
			/// Reset the model and its data.
			/// </summary>
			/// <param name="reason">The reset reason.</param>
			virtual void Reset(const std::string& reason) = 0;

			RTTR_ENABLE(IObject)
			RTTR_REGISTRATION_FRIEND
		};
	}
}