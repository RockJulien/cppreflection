#pragma once

#include "IDataModel.hpp"

namespace Core
{
	namespace Models
	{
		/// <summary>
		/// Author: jlarbi
		/// 
		/// Base strong type interface for all config models (non mutable over app lifecycle).
		/// </summary>
		class IConfigModel : public virtual IDataModel
		{
		protected:
			IConfigModel() = default;

		public:
			virtual ~IConfigModel() = 0;

			RTTR_ENABLE(IDataModel)
			RTTR_REGISTRATION_FRIEND
		};
	}
}