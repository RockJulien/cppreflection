#include "IDataModel.hpp"

#include <rttr/registration.h>

using namespace Core::Models;

RTTR_PLUGIN_REGISTRATION
{
    // Involved namespace(s).
    using namespace rttr;

    // Indicate what the class is made of for reflection potential purpose on it.
    registration::class_<IDataModel>("Core::Models::IDataModel")
                .property_readonly("GetName", &IDataModel::GetName, registration::public_access)
                .property_readonly("IsConfig", &IDataModel::IsConfig, registration::public_access)
                .method("Reset", &IDataModel::Reset, registration::public_access);
}