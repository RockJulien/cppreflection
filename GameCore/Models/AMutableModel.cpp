#include "AMutableModel.hpp"

#include <rttr/registration.h>

using namespace Core::Models;

bool AMutableModel::IsConfig() const
{
    return false;
}

RTTR_PLUGIN_REGISTRATION
{
    // Involved namespace(s).
    using namespace rttr;

    // Indicate what the class is made of for reflection potential purpose on it.
    registration::class_<AMutableModel>("Core::Models::AMutableModel");
}