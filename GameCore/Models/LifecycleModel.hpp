#pragma once

#include "AMutableModel.hpp"
#include "Event.hpp"

namespace Core
{
	namespace Models
	{
		/// <summary>
		/// Author: jlarbi
		/// 
		/// Represents the app lifecycle data model.
		/// </summary>
		class LifecycleModel : public AMutableModel
		{
		public:

			Event<std::string> GameInitializing;
			//Event<JobSequenceProgress> GameInitializationProgress;
			Event<std::string> GameInitialized;
			Event<> GamePaused;
			Event<> GameResumed;
			Event<std::string> GameReseting;
			Event<std::string> GameClosing;
		};
	}
}