#include "AConfigModel.hpp"

#include <rttr/registration.h>

using namespace Core::Models;

bool AConfigModel::IsConfig() const
{
	return true;
}

RTTR_PLUGIN_REGISTRATION
{
    // Involved namespace(s).
    using namespace rttr;

    // Indicate what the class is made of for reflection potential purpose on it.
    registration::class_<AConfigModel>("Core::Models::AConfigModel");
}