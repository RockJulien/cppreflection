#pragma once

#include "IDataModel.hpp"

namespace Core
{
	namespace Models
	{
		/// <summary>
		/// Author: jlarbi
		/// 
		/// Base strong type interface for all state models (mutable over app lifecycle).
		/// </summary>
		class IMutableModel : public virtual IDataModel
		{
		protected:
			IMutableModel() = default;

		public:
			virtual ~IMutableModel() = 0;

			RTTR_ENABLE(IDataModel)
			RTTR_REGISTRATION_FRIEND
		};
	}
}