#pragma once

#include "ADataModel.hpp"
#include "IConfigModel.hpp"

namespace Core
{
	namespace Models
	{
		/// <summary>
		/// Author: jlarbi
		/// 
		/// Base abstract class for all config models (non mutable over app lifetime)
		/// </summary>
		class AConfigModel : public ADataModel, public IConfigModel
		{
		protected:

			AConfigModel() = default;

		public:

			virtual ~AConfigModel() = 0;

			/// <summary>
			/// Gets the flag indicating whether the model is a config one or not.
			/// </summary>
			virtual bool IsConfig() const override final;

			RTTR_ENABLE(ADataModel, IConfigModel)
			RTTR_REGISTRATION_FRIEND
		};
	}
}