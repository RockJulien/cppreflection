#pragma once

#define constant inline static const

#include <string>
#include <functional>
#include <list>
#include <algorithm>
#include <utility>
#include <atomic>
#include <mutex>
#include <future>
#include <exception>
#include <condition_variable>
#include <rttr/type>
#include <rttr/registration_friend.h>
#include <gamecore_export.h>

namespace Core
{
	/// <summary>
	/// Author: jlarbi
	/// 
	/// Constants class having const and static common data.
	/// </summary>
	class GAMECORE_EXPORT Constants
	{
	public:

		constant std::string USER_QUIT_REASON = "User";
		constant std::string CRASH_QUIT_REASON = "Crash";
		
		constant std::string USER_RESTART_REASON = "User";
		constant std::string SETTINGS_RESTART_REASON = "Settings";
		constant std::string DESYNC_RESTART_REASON = "Desync";
		
		constant std::string USER_START_REASON = "User";
		constant std::string RESTART_START_REASON = "Restart";

		RTTR_ENABLE()
	};
}