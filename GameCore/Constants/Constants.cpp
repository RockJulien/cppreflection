
#include <Constants.hpp>

#include <rttr/registration.h>

using namespace Core;

RTTR_PLUGIN_REGISTRATION
{
    // Involved namespace(s).
    using namespace rttr;

    // Indicate what the class is made of for reflection potential purpose on it.
    registration::class_<Constants>("Core::Constants")
                    .property_readonly("USER_QUIT_REASON", &Constants::USER_QUIT_REASON, registration::public_access)
                    .property_readonly("CRASH_QUIT_REASON", &Constants::CRASH_QUIT_REASON, registration::public_access)
                    .property_readonly("USER_RESTART_REASON", &Constants::USER_RESTART_REASON, registration::public_access)
                    .property_readonly("SETTINGS_RESTART_REASON", &Constants::SETTINGS_RESTART_REASON, registration::public_access)
                    .property_readonly("DESYNC_RESTART_REASON", &Constants::DESYNC_RESTART_REASON, registration::public_access)
                    .property_readonly("USER_START_REASON", &Constants::USER_START_REASON, registration::public_access)
                    .property_readonly("RESTART_START_REASON", &Constants::RESTART_START_REASON, registration::public_access);
}