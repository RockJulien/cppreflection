#include "Exceptions.hpp"

#include <rttr/registration.h>

using namespace Core::Exceptions;

const char* IsEmptyException::what() const throw()
{
	return IS_EMPTY_MESSAGE.c_str();
}

RTTR_PLUGIN_REGISTRATION
{
	// Involved namespace(s).
	using namespace rttr;

	// Indicate what the class is made of for reflection potential purpose on it.

	registration::class_<IsEmptyException>("Core::Exceptions::IsEmptyException")
				.constructor<>(registration::public_access)
				;
}