#pragma once

#include <Base/IObject.hpp>

namespace Core
{
	namespace Exceptions
	{
		/// <summary>
		/// Author: jlarbi
		/// 
		/// Exception thrown when anything empty-able end up empty but is tried to be accessed.
		/// </summary>
		class GAMECORE_EXPORT IsEmptyException : public std::exception, public IObject
		{
		private:

			constant std::string IS_EMPTY_MESSAGE = "You are trying to access an empty element.";

		public:

			const char* what() const throw();

			RTTR_ENABLE(IObject)
			RTTR_REGISTRATION_FRIEND
		};
	}
}