#include "IService.hpp"

#include <rttr/registration.h>

using namespace Core::Services;

RTTR_PLUGIN_REGISTRATION
{
	// Involved namespace(s).
	using namespace rttr;

	// Indicate what the class is made of for reflection potential purpose on it.
	registration::class_<IService>("Core::Services::IService")
					.method("Reset", &IService::Reset, registration::public_access)
					(
						parameter_names("reason")
					);
}