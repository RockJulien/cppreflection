#include "AService.hpp"

#include <rttr/registration.h>

using namespace Core::Services;

void AService::Reset(const std::string& reason)
{
	OnReset(reason);
}

void AService::OnReset(const std::string& reason)
{
	// Nothing to do ...
}

RTTR_PLUGIN_REGISTRATION
{
	// Involved namespace(s).
	using namespace rttr;

	// Indicate what the class is made of for reflection potential purpose on it.
	registration::class_<AService>("Core::Services::AService")
					.method("OnReset", &AService::OnReset, registration::protected_access)
					(
						parameter_names("reason")
					);
}
