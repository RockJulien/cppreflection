#pragma once

#include <Commands/ICommandHandler.hpp>

namespace Core
{
    namespace Services
    {
        /// <summary>
        /// Author : jlarbi
        /// </summary>
        class IService : public Commands::ICommandHandler
        {
        protected:

            IService() = default;

        public:

            virtual ~IService() = 0;

            /// <summary>
            /// Reset the service and its data if needed.
            /// </summary>
            /// <param name="reason">The reset reason.</param>
            virtual void Reset(const std::string& reason) = 0;

            RTTR_ENABLE(Commands::ICommandHandler)
            RTTR_REGISTRATION_FRIEND
        };
    }
}