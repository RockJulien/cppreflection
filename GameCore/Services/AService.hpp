#pragma once

#include <IService.hpp>

namespace Core
{
	namespace Services
	{
		/// <summary>
		/// Author: jlarbi
		/// 
		/// Base abstract service class.
		/// </summary>
		class AService : public IService
		{
		protected:

			AService() = default;

			/// <summary>
			/// Reset the service and its data if needed.
			/// </summary>
			/// <param name="reason">The reset reason.</param>
			virtual void OnReset(const std::string& reason);

		public:

			virtual ~AService() = 0;

			/// <summary>
			/// Reset the service and its data if needed.
			/// </summary>
			/// <param name="reason">The reset reason.</param>
			virtual void Reset(const std::string& reason) override final;

			RTTR_ENABLE(IService)
			RTTR_REGISTRATION_FRIEND
		};
	}
}