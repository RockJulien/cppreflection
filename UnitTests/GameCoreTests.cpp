#define BOOST_TEST_MODULE GameCoreTests
#include <boost/test/unit_test.hpp>
#include <rttr/type>
#include <Base/StreamColor.hpp>
#include <Base/Timer.hpp>
#include <Containers/ConcurrentStack.hpp>
#include <Containers/HashSet.hpp>

#ifdef NDEBUG
static rttr::string_view gameCoreLibName("GameCore");
#else
static rttr::string_view gameCoreLibName("GameCore_d");
#endif

static rttr::enum_flags<rttr::filter_item> memberFilter = rttr::filter_item::instance_item | rttr::filter_item::non_public_access | rttr::filter_item::public_access | rttr::filter_item::static_item;

static void PrintCurrentTestExecuting()
{
	std::cout << "Running Test " << boost::unit_test::framework::current_test_case().p_name << " ..." << std::endl;
}

static void PrintTypeInfo(const rttr::type& objectType)
{
	auto baseTypes = objectType.get_base_classes();
	bool hasBaseClasses = false;
	std::cout << "Base types :" << std::endl;
	for (auto& baseType : baseTypes)
	{
		std::cout << "    - " << baseType.get_name() << std::endl;
		hasBaseClasses = true;
	}

	if (!hasBaseClasses)
	{
		std::cout << "    - has no base classes..." << std::endl;
	}

	auto derivedTypes = objectType.get_derived_classes();
	bool hasDerivedClasses = false;
	std::cout << "Derived types :" << std::endl;
	for (auto& derivedType : derivedTypes)
	{
		std::cout << "    - " << derivedType.get_name() << std::endl;
		hasDerivedClasses = true;
	}

	if (!hasDerivedClasses)
	{
		std::cout << "    - has no derived classes..." << std::endl;
	}

	auto properties = objectType.get_properties(memberFilter);
	bool hasProperties = false;
	std::cout << "Properties :" << std::endl;
	for (auto& prop : properties)
	{
		std::cout << "    - (type: " << prop.get_type().get_name() << ")";
		std::cout << " name: " << prop.get_name() << std::endl;
		hasProperties = true;
	}

	if (!hasProperties)
	{
		std::cout << "    - has no properties..." << std::endl;
	}

	std::cout << "Methods :" << std::endl;
	bool hasMethods = false;
	for (auto& meth : objectType.get_methods(memberFilter))
	{
		std::cout << "    - name: " << meth.get_name() << std::endl;
		std::cout << "    - signature: " << meth.get_signature() << std::endl;
		for (auto& info : meth.get_parameter_infos())
		{
			std::cout << "        - param " << info.get_index() << ": name: " << info.get_name() << std::endl;
		}
		hasMethods = true;
	}

	if (!hasMethods)
	{
		std::cout << "    - has no methods..." << std::endl;
	}
}

BOOST_AUTO_TEST_CASE(GameCoreLibraryLoadTest)
{
	using namespace rttr;

	PrintCurrentTestExecuting();

	library lib(gameCoreLibName); // load the GameCore plugin
	
	BOOST_CHECK(lib.load());

	BOOST_CHECK(lib.unload());
}

BOOST_AUTO_TEST_CASE(GameCoreLibraryGetTypesTest)
{
	using namespace rttr;

	PrintCurrentTestExecuting();

	library lib(gameCoreLibName); // load the GameCore plugin

	BOOST_CHECK(lib.load());

	{
		// print all classes contained in the library
		std::cout << "Plugin types :" << std::endl;
		for (auto type : lib.get_types())
		{
			BOOST_CHECK(type.is_valid());

			if (type.is_class() && !type.is_wrapper())
			{
				BOOST_CHECK(!type.get_name().to_string().empty());

				std::cout << "    - " << type.get_name() << std::endl;
			}
		}
	}

	BOOST_CHECK(lib.unload());
}

BOOST_AUTO_TEST_CASE(GameCoreLibraryIObjectTest)
{
	using namespace rttr;

	PrintCurrentTestExecuting();

	library lib(gameCoreLibName); // load the GameCore plugin

	BOOST_CHECK(lib.load());

	{
		type objectType = type::get_by_name("Core::IObject");
		BOOST_CHECK(objectType.is_class());
		BOOST_CHECK(objectType.get_constructors().size() == 0); // Interface.
		BOOST_CHECK(objectType.get_base_classes().size() == 0);
		BOOST_CHECK(objectType.get_derived_classes().size() > 0);
		BOOST_CHECK(objectType.get_properties().size() == 0);
		BOOST_CHECK(objectType.get_methods().size() > 0);
		PrintTypeInfo(objectType);
	}

	BOOST_CHECK(lib.unload());
}

BOOST_AUTO_TEST_CASE(GameCoreLibraryConstantsTest)
{
	using namespace rttr;

	PrintCurrentTestExecuting();

	library lib(gameCoreLibName); // load the GameCore plugin

	BOOST_CHECK(lib.load());

	{
		type objectType = type::get_by_name("Core::Constants");
		BOOST_CHECK(objectType.is_class());
		BOOST_CHECK(objectType.get_constructors().size() == 0);
		BOOST_CHECK(objectType.get_base_classes().size() == 0);
		BOOST_CHECK(objectType.get_derived_classes().size() == 0);
		BOOST_CHECK(objectType.get_properties().size() == 7);
		BOOST_CHECK(objectType.get_methods().size() == 0);
		PrintTypeInfo(objectType);
	}

	BOOST_CHECK(lib.unload());
}

BOOST_AUTO_TEST_CASE(GameCoreTimerTest)
{
	using namespace rttr;
	using namespace Core;

	PrintCurrentTestExecuting();

	library lib(gameCoreLibName); // load the GameCore plugin

	BOOST_CHECK(lib.load());

	{
		type objectType = type::get_by_name("Core::Timer");
		BOOST_CHECK(objectType.is_valid());
		BOOST_CHECK(objectType.is_class());
		BOOST_CHECK(objectType.get_constructors().size() == 1);
		BOOST_CHECK(objectType.get_base_classes().size() == 1);
		BOOST_CHECK(objectType.get_derived_classes().size() == 0);
		BOOST_CHECK(objectType.get_properties().size() == 1);
		BOOST_CHECK(objectType.get_methods().size() == 5);
		PrintTypeInfo(objectType);

		auto timer = objectType.create({ false, 1000u });
		BOOST_CHECK(timer.is_valid());

		variant return_value = objectType.invoke("Start", timer, {});
		BOOST_CHECK(return_value.is_valid());
		BOOST_CHECK(return_value.to_bool());
	}

	BOOST_CHECK(lib.unload());

	int test = -1;
	std::atomic_bool isDone = false;
	Core::Timer timer(false);
	timer.Tick += [&test, &isDone]() { test = 50; isDone = true; };
	timer.Start();

	while (!isDone)
	{
		// Wait.
	}

	BOOST_CHECK(test == 50);

	// Some inheritance tests.
	Core::IObject* baseTimer = &timer;

	BOOST_CHECK(baseTimer->GetType() == timer.GetType());
	BOOST_CHECK(baseTimer->ToString() == "Core::Timer");
	BOOST_CHECK(timer.ToString() == "Core::Timer");
	BOOST_CHECK(timer.Is<IObject>());
}

BOOST_AUTO_TEST_CASE(GameCoreConcurrentStackTest)
{
	using namespace rttr;
	using namespace Core;

	PrintCurrentTestExecuting();

	Core::Containers::ConcurrentStack<int> stack;
	for (int i = 0; i < 5; ++i)
	{
		stack.Push(i);
	}

	int counter = 4;
	while (!stack.IsEmpty())
	{
		auto item = stack.Pop();
		BOOST_CHECK((*item) == counter--);
		std::cout << Core::LogWithColor("32", (*item)) << std::endl;
	}
}

BOOST_AUTO_TEST_CASE(GameCoreHashsetTest)
{
	PrintCurrentTestExecuting();

	Core::Containers::Hashset<int> hashset;
	hashset.Add(0);
	hashset.Add(1);
	hashset.Add(2);
	hashset.Add(3);
	hashset.Add(4);

	BOOST_CHECK(hashset.Count() == 5);

	// Iteration test.
	int counter = 0;
	for (auto i : hashset)
	{
		BOOST_CHECK(i == counter++);
	}

	hashset.Remove(2);

	BOOST_CHECK(hashset.Count() == 4);

	// Should only add 2, 5, 6 to hashset having 0, 1, 3, 4.
	// And is now equal to 0, 1, 2, 3, 4, 5, 6
	hashset.UnionWith({ 2, 3, 4, 5, 6 });

	BOOST_CHECK(hashset.Count() == 7);

	BOOST_CHECK(hashset.Overlaps({ 1, 2, 3}));

	// Should remove 5, 6 from hashset
	// And is now equal to 0, 1, 2, 3, 4
	hashset.ExceptWith({5, 6});

	BOOST_CHECK(hashset.Count() == 5);

	// Remove all from hashset not being in that other list.
	// And is now equal to 1, 3 but only if contained too.
	// This means IntersectWith(Empty) or With nothing in common clears hashset.
	hashset.IntersectWith({1, 3});

	BOOST_CHECK(hashset.Count() == 2);

	hashset.Clear();

	BOOST_CHECK(hashset.Count() == 0);
}
